package com.codebrewers.server.shared;

import com.codebrewers.server.models.User;

public class User_Password {
    private User user;
    private String password;

    public User_Password(User user, String password) {
        this.user = user;
        this.password = password;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
